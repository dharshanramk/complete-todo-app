import express from 'express';
import { v4 as uuidv4 } from 'uuid';


const router = express.Router();

let users = [ ]

router.get('/', (req, res) => { 
   
    res.send(users);
});

router.post('/', (req, res) => { 
    const user = req.body;                                                                          

    users.push({ ...user, Day:uuidv4() });

    res.send(`Dayname is  ${user.Dayname} `);                                                   //Day - unique which is used to delete , update and get
});
router.get('/:Day', (req, res) => { 
        const {Day} = req.params;

        const foundUser = users.find((user) => user.Day == Day);
        res.send(foundUser);
    
});
router.delete('/:Day', (req, res) => { 
    const {Day} = req.params;

    users = users.filter((user) => user.Day != Day);
    res.send(`Dayname with daycode ${Day} deleted`);

});

router.patch('/:Day', (req, res) => { 
    const {Day} = req.params;
    const {Dayname ,Taskname1,Taskname2 } = req.body;

    const user = users.find((user) => user.Day == Day);

    if (Dayname) user.Dayname = Dayname;
    if (Taskname1)user.Taskname1 = Taskname1;
    if (Taskname2) user.Taskname2 = Taskname2;
    
    res.send(`Dayname with daycode ${Day} has been updated`);

});

export default router;